<?php
/**
 * Load files
 *
 * @package My_Cafe
 */

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';




/**
 * Include default theme options
 */
require_once get_template_directory() . '/inc/customizer/default.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Hook for home page content
 */
require get_template_directory() . '/inc/hooks/home-content.php';

/**
 * Hook for page header
 */
require get_template_directory() . '/inc/hooks/page-header.php';

/**
 * Hook for footer social media
 */
require get_template_directory() . '/inc/hooks/social-media.php';

/**
 * bootstrap navigation
 */
//require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Function limit the number of words.
 */
function mycafe_limit_words($string, $word_limit) {

	$words = explode(' ', $string, ($word_limit + 1));

	if(count($words) > $word_limit) {

		if(count($words) > $word_limit) {

			array_pop($words);

			return implode(' ', $words).'...';
			
		}
	} else {  

		return $string;

	}

}


/**
 * Function to check widget status
 */
 if ( ! function_exists( 'mycafe_widget_count' ) ) :

 function mycafe_widget_count( $sidebar_names ){

    $status = 0;

    foreach ($sidebar_names as $sidebar) {
      
        if( is_active_sidebar( $sidebar )){
            $status = $status+1;
        }
    }

    return $status;        
 }

endif;
