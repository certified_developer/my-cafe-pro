<?php
/**
 * Theme Options.
 *
 * @package My_Cafe
 */

$default = mycafe_default_theme_options();

/**=========== Option Panel ===========**/
$wp_customize->add_panel(
    'mycafe_basic_panel', 
    array(
        'title'             => __( 'Theme Options', 'mycafe' ),
        'priority'          => 200, 
    )
);

/**=========== Header Setting - start ===========**/
$wp_customize->add_section(
    'mycafe_header', 
    array(    
        'title'             => __( 'Header', 'mycafe' ),
        'panel'             => 'mycafe_basic_panel', 
    )
);  

/*----------- Sticky header -----------*/
$wp_customize->add_setting(
    'mycafe[sticky_header]',
    array(
        'default'           => $default['sticky_header'],       
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[sticky_header]', 
    array(
        'label'             => __( 'Enable sticky header', 'mycafe' ),
        'section'           => 'mycafe_header',   
        'settings'          => 'mycafe[sticky_header]',     
        'type'              => 'checkbox',
    )
);
/**=========== Header Setting - end ===========**/






/**=========== Slider Settings - start ===========**/
$wp_customize->add_section(
    'mycafe_slider', 
    array(    
        'title'             => __( 'Slider', 'mycafe' ),
        'panel'             => 'mycafe_basic_panel'    
    )
);    

/*----------- Enable/Disable Slider -----------*/
$wp_customize->add_setting(
    'mycafe[slider_enable]', 
    array(
        'default'           => $default['slider_enable'],       
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[slider_enable]', 
    array(
        'label'             => __( 'Enable slider OR banner image', 'mycafe' ),
        'section'           => 'mycafe_slider',   
        'settings'          => 'mycafe[slider_enable]',     
        'type'              => 'checkbox'
    )
);  

/*----------- Show/Hide slider excerpt -----------*/
$wp_customize->add_setting(
    'mycafe[slider_excerpt_enable]', 
    array(
        'default'           => $default['slider_excerpt_enable'],       
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[slider_excerpt_enable]', 
    array(
        'label'             => __( 'Display excerpt', 'mycafe' ),
        'section'           => 'mycafe_slider',   
        'settings'          => 'mycafe[slider_excerpt_enable]',     
        'type'              => 'checkbox',
        'active_callback'   => 'mycafe_main_slider',
    )
);

/*----------- Show/Hide slider button -----------*/
$wp_customize->add_setting(
    'mycafe[slider_btn_enable]', 
    array(
        'default'           => $default['slider_btn_enable'],       
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[slider_btn_enable]', 
    array(
        'label'             => __( 'Display read button', 'mycafe' ),
        'section'           => 'mycafe_slider',   
        'settings'          => 'mycafe[slider_btn_enable]',     
        'type'              => 'checkbox',
        'active_callback'   => 'mycafe_main_slider',
    )
);

/*----------- Slider type -----------*/
$wp_customize->add_setting(
    'mycafe[main_slider_type]', 
    array(
        'default'           => $default['main_slider_type'],        
        'sanitize_callback' => 'mycafe_sanitize_select'
    )
);

$wp_customize->add_control(
    'mycafe[main_slider_type]', 
    array(      
        'label'             => __( 'Select slider type', 'mycafe' ),
        'section'           => 'mycafe_slider',
        'settings'          => 'mycafe[main_slider_type]',
        'type'              => 'radio',
        'choices'           => array(
            'slider'        => __( 'Slider', 'mycafe' ),
            'banner-image'  => __( 'Banner image', 'mycafe' ),              
        ),
        'active_callback'   => 'mycafe_main_slider',
    )
);  

/*----------- Slider category -----------*/
$wp_customize->add_setting(
    'mycafe[slider_cat]', 
    array(
        'default'           => $default['slider_cat'],        
        'sanitize_callback' => 'mycafe_sanitize_number'
    )
);

$wp_customize->add_control(
    new MyCafe_Customize_Category_Control(
        $wp_customize,
        'mycafe[slider_cat]',
        array(
            'label'         => __( 'Category for slider', 'mycafe'  ),
            'description'   => __( 'Posts of selected category will be used as sliders', 'mycafe'  ),
            'settings'      => 'mycafe[slider_cat]',
            'section'       => 'mycafe_slider',
            'active_callback'   => 'mycafe_slider_category',        
        )
    )
);

/*----------- Banner image -----------*/
$wp_customize->add_setting(
    'mycafe[banner_image]', 
    array(
        'default'           => $default['banner_image'],             
        'sanitize_callback' => 'mycafe_sanitize_number',          
    )
);  

$wp_customize->add_control(
    'mycafe[banner_image]', 
    array(
        'label'             => __( 'Banner image', 'mycafe' ),   
        'description'       => __( 'Enter the ID of post to use as a banner image.', 'mycafe'  ), 
        'settings'          => 'mycafe[banner_image]',
        'section'           => 'mycafe_slider',
        'type'              => 'text',
        'active_callback'   => 'mycafe_banner_category',         
    )
);
/**=========== Slider Settings - end ===========**/









/**=========== Home Section - start ===========**/
$wp_customize->add_section(
    'mycafe_home', 
    array(    
        'title'       => __( 'Home Sections', 'mycafe' ),
        'panel'       => 'mycafe_basic_panel'    
    )
);

/**=========== Show home page content ===========**/
$wp_customize->add_setting(
    'mycafe[home_content]', 
    array(
        'default'           => $default['home_content'],       
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[home_content]', 
    array(
        'label'       => __( 'Show home content', 'mycafe' ),
        'description' => __( 'Check this box to show page content in home page', 'mycafe' ),
        'section'     => 'mycafe_home',   
        'settings'    => 'mycafe[home_content]',      
        'type'        => 'checkbox'
    )
);


// About us
$wp_customize->add_setting(
    'mycafe[about_us]', 
    array(
        'default'           => $default['about_us'],             
        'sanitize_callback' => 'mycafe_sanitize_number',          
    )
);  

$wp_customize->add_control(
    'mycafe[about_us]', 
    array(
        'label'             => __( 'About us', 'mycafe' ),   
        'description'       => __( 'Leave blank to hide this section', 'mycafe'  ), 
        'settings'          => 'mycafe[about_us]',
        'section'           => 'mycafe_home',
        'type'              => 'dropdown-pages',         
    )
);


// Our services
$wp_customize->add_setting(
    'mycafe[our_services]', 
    array(
        'default'           => $default['our_services'],      
        'sanitize_callback' => 'mycafe_sanitize_number'
        )
    );  

$wp_customize->add_control(
    new MyCafe_Customize_Category_Control(
        $wp_customize,
        'mycafe[our_services]',
        array(
            'label'       => __( 'Our services', 'mycafe' ),
            'description' => __( 'Leave blank to hide this section.', 'mycafe' ),
            'settings'    => 'mycafe[our_services]',
            'section'     => 'mycafe_home',                    
            )
        )
    );

// Testimonial
$wp_customize->add_setting(
    'mycafe[our_blog]', 
    array(
        'default'           => $default['our_blog'],      
        'sanitize_callback' => 'mycafe_sanitize_number'
        )
    );  

$wp_customize->add_control(
    new MyCafe_Customize_Category_Control(
        $wp_customize,
        'mycafe[our_blog]',
        array(
            'label'       => __( 'Our blog', 'mycafe' ),
            'description' => __( 'Leave blank to hide this section.', 'mycafe' ),
            'settings'    => 'mycafe[our_blog]',
            'section'     => 'mycafe_home',                    
            )
        )
    );


/**=========== Home Section - end ===========**/


/**=========== Social Media Options - start ===========**/
$wp_customize->add_section(
    'mycafe_social', 
    array(    
        'title'       => __( 'Social Media', 'mycafe' ),
        'panel'       => 'mycafe_basic_panel'    
        ));

/*----------- Social link text field -----------*/
$social_options = array('facebook', 'twitter', 'google_plus', 'instagram', 'linkedin', 'pinterest', 'youtube', 'vimeo', 'flickr', 'behance', 'dribbble', 'tumblr', 'github' );

foreach($social_options as $social) {

    $social_name = ucwords(str_replace('_', ' ', $social));

    $label = '';

    switch ($social) {

        case 'facebook':
        $label = __('Facebook', 'mycafe' );
        break;

        case 'twitter':
        $label = __( 'Twitter', 'mycafe'  );
        break;

        case 'google_plus':
        $label = __( 'Google Plus', 'mycafe'  );
        break;

        case 'instagram':
        $label = __( 'Instagram', 'mycafe'  );
        break;

        case 'linkedin':
        $label = __( 'LinkedIn', 'mycafe'  );
        break;

        case 'pinterest':
        $label = __( 'Pinterest', 'mycafe'  );
        break;

        case 'youtube':
        $label = __( 'Youtube', 'mycafe'  );
        break;

        case 'vimeo':
        $label = __( 'vimeo', 'mycafe'  );
        break;

        case 'flickr':
        $label = __( 'Flickr', 'mycafe'  );
        break;

        case 'behance':
        $label = __( 'Behance', 'mycafe'  );
        break;

        case 'dribbble':
        $label = __( 'Dribbble', 'mycafe'  );
        break;

        case 'tumblr':
        $label = __( 'Tumblr', 'mycafe'  );
        break;

        case 'github':
        $label = __( 'Github', 'mycafe'  );
        break;

    }
    
    $wp_customize->add_setting( 'mycafe['.$social.']', array(
        'sanitize_callback'     => 'esc_url_raw',
        'sanitize_js_callback'  =>  'esc_url_raw'
        ));

    $wp_customize->add_control( 'mycafe['.$social.']', array(
        'label'     => $label,
        'type'      => 'text',
        'section'   => 'mycafe_social',
        'settings'  => 'mycafe['.$social.']'
        ));
}
/**=========== Social Media Options - end ===========**/


/**=========== General setting - start ===========**/
$wp_customize->add_section(
    'mycafe_general', 
    array(    
        'title'       => __('General Setting', 'mycafe' ),
        'panel'       => 'mycafe_basic_panel'    
    )
);

/**=========== Page layout ===========**/
$wp_customize->add_setting( 
    'mycafe[sidebar]',
    array(
        'default'           => $default['sidebar'],
        'sanitize_callback' => 'mycafe_sanitize_select',
    )
);
$wp_customize->add_control( 'mycafe[sidebar]',
    array(
        'label'       => esc_html__( 'Page layout', 'mycafe'  ),
        'section'     => 'mycafe_general',   
        'settings'    => 'mycafe[sidebar]',
        'type'        => 'radio',
        'choices'     => array(
            'right'     => esc_html__( 'Right sidebar', 'mycafe'  ),
            'left'      => esc_html__( 'Left sidebar', 'mycafe'  ),
            'no_side'   => esc_html__( 'No sidebar', 'mycafe'  ),
            )
    )
);

/**=========== Enable/Disable - post date ===========**/
$wp_customize->add_setting(
    'mycafe[enable_post_date]', 
    array(
        'default'           => $default['enable_post_date'],     
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[enable_post_date]', 
    array(
        'label'       => __('Enable post date', 'mycafe' ),    
        'settings'    => 'mycafe[enable_post_date]',
        'section'     => 'mycafe_general',
        'type'        => 'checkbox',
    )
);

/**=========== Enable/Disable - post author ===========**/
$wp_customize->add_setting(
    'mycafe[enable_post_author]', 
    array(
        'default'           => $default['enable_post_author'],     
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[enable_post_author]', 
    array(
        'label'       => __('Enable post author', 'mycafe' ),    
        'settings'    => 'mycafe[enable_post_author]',
        'section'     => 'mycafe_general',
        'type'        => 'checkbox',
    )
);

/**=========== Enable/Disable - post meta ===========**/
$wp_customize->add_setting(
    'mycafe[enable_post_meta]', 
    array(
        'default'           => $default['enable_post_meta'],     
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[enable_post_meta]', 
    array(
        'label'       => __('Enable post meta', 'mycafe' ),    
        'settings'    => 'mycafe[enable_post_meta]',
        'section'     => 'mycafe_general',
        'type'        => 'checkbox',
    )
);

/**=========== General setting - end ===========**/


/**=========== Footer Options - start ===========**/
$wp_customize->add_section(
    'mycafe_footer', 
    array(    
        'title'       => __( 'Footer', 'mycafe' ),
        'panel'       => 'mycafe_basic_panel'    
    )
); 

/**=========== Enable/Disable - social media ===========**/ 
$wp_customize->add_setting(
    'mycafe[enable_social_media]', 
    array(
        'default'           => $default['enable_social_media'],     
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[enable_social_media]', 
    array(
        'label'       => __( 'Enable social media', 'mycafe' ),    
        'settings'    => 'mycafe[enable_social_media]',
        'section'     => 'mycafe_footer',
        'type'        => 'checkbox',
    )
);     

/**=========== Copyright text ===========**/
$wp_customize->add_setting(
    'mycafe[copyright_text]', 
    array(
      'default'           => $default['copyright_text'],  
      'sanitize_callback' => 'mycafe_sanitize_html',
    )
);

$wp_customize->add_control(
    'mycafe[copyright_text]', 
        array(
        'label'       => __( 'Copyright text', 'mycafe' ),    
        'settings'    => 'mycafe[copyright_text]',
        'section'     => 'mycafe_footer',
        'type'        => 'textarea'
    )
);

/**=========== Enable/Disable - scroll to top ===========**/
$wp_customize->add_setting(
    'mycafe[enable_scroll_top]', 
    array(
        'default'           => $default['enable_scroll_top'],     
        'sanitize_callback' => 'mycafe_sanitize_checkbox'
    )
);

$wp_customize->add_control(
    'mycafe[enable_scroll_top]', 
    array(
        'label'       => __( 'Enable scroll to top', 'mycafe' ),    
        'settings'    => 'mycafe[enable_scroll_top]',
        'section'     => 'mycafe_footer',
        'type'        => 'checkbox'
    )
);
/**=========== Footer Options - end ===========**/
